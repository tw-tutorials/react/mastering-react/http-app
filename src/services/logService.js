import * as Sentry from '@sentry/browser';

function init() {
    Sentry.init({
        dsn: "https://f26ba584844541bca1e7f3514bc4d6fa@sentry.io/1419854",
        release: '1-0-0',
        environment: 'development-test'
    });
}

function log(error) {
    Sentry.captureException(error);
}

export default { init, log };